---
title: Email to client to perform analysis
keywords: Account Recovery
last_updated: August 18, 2021
tags: [account_recovery_templates, templates]
summary: "Use this email to ask the client's permission to perform analysis on their digital assets in order to detect compromise, or in order to find further evidence or details about a known compromise."
sidebar: mydoc_sidebar
permalink: 411-Email_to_client_to_perform_Analysis.html
folder: mydoc
conf: Public
ref: Email_to_client_to_perform_Analysis
lang: en
---


# Incident Analysis
## Incident analysis in order to identify the root cause of an incident.

### Body

Dear {{ beneficiary name }},

There is a need to perform further analysis in order to determine the root cause of the incident.

Knowing the root cause of the incident would help us understand how your {{ account/device }} was compromised. This would allow us to help you prevent a similar incident from happening again in the future. This analysis may also help prevent similar incidents from happening to other at-risk users.

Performing the analysis would require us to send you instructions to do some checks on your {{ account/device }}. Alternatively, we can set up a call with a member of our team to perform a live check of your {{ account/device }}.

Please let us know if this would be possible, and if so, we will provide you with next steps.

Thanks,
{{ incident handler name }}



* * *


### Related Articles

- [Article #412: Account Compromise Process](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/Account_Recovery/412-Account_Compromise_Process.md)
