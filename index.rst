.. this is the index file that tells Sphinx/RtD how to build the static site at https://maggie-documentation.readthedocs.io/en/latest/ .


Test: Community Documentation
=============================

This is a test instance of the Access Now Digital Security Helpline's Community Documentation.
You can contribute to our documentation through our Gitlab repository.

Our documentation is organized by subject. Scroll down to see all of the topics covered in our documentation.

.. toctree::
   :maxdepth: 2

   Account Security <landing_pages/account_security>
   Anonymity and Circumvention <landing_pages/anonymity_circumvention>
   Browsing Security <landing_pages/browsing_security>
   Censorship <landing_pages/censorship>
   Data Leaks <landing_pages/data_leaks>
   DDoS Attacks <landing_pages/ddos>
   Device and Data Security <landing_pages/devices_data_security>
   Direct Interventions <landing_pages/direct_interventions>
   Documentation <landing_pages/documentation>
   Fake Domains <landing_pages/fake_domain>
   Forensics <landing_pages/forensics>
   Harassment <landing_pages/harassment>
   Helpline Procedures <landing_pages/helpline_procedures>
   Infrastructure <landing_pages/infrastructure>
   Organizational Security <landing_pages/organization_security>
   Phishing and Suspicious Emails <landing_pages/phishing_suspicious_email>
   Secure Communications <landing_pages/secure_communications>
   Vulnerabilities and Malware <landing_pages/vulnerabilities_and_malware>
   Website Defacement <landing_pages/defacement>
   Web Vulnerabilities <landing_pages/web_vulnerabilities>

Templates
=========

You can find all of our email templates below. These templates are organized by subject.

.. toctree::
   :maxdepth: 2

   Templates <landing_pages/templates>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
