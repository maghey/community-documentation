Templates
=========

Here you'll find all of our email templates.
You can contribute to our documentation through our Gitlab repository.

.. toctree::
   :maxdepth: 1

   Account Recovery Templates <landing_pages/template_landing_pages/account_recovery_templates>
   Censorship Templates <../landing_pages/template_landing_pages/censorship_templates>
   Device and Data Security Templates <../../landing_pages/template_landing_pages/devices_data_security_templates>
   Fake Domain Templates <template_landing_pages/fake_domain_templates>
   Harassment Templates <template_landing_pages/harassment_templates>
   Helpline Procedures Templates <template_landing_pages/helpline_procedures_templates>
   Infrastructure Templates <template_landing_pages/infrastructure_templates>
   Organizational Security Templates <template_landing_pages/organization_security_templates>
   Phishing and Suspicious Email Templates <template_landing_pages/phishing_suspicious_email_templates>
   Secure Communications Templates <template_landing_pages/secure_communications_templates>
   Vulnerabilities and Malware Templates <template_landing_pages/vulnerabilities_and_malware_templates>
