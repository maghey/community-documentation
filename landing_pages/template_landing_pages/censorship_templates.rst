Censorship Templates
====================

Here you'll find all of our email templates related to censorship.
You can contribute to our documentation through our Gitlab repository.

.. toctree::
   :maxdepth: 1

   <../../pages/mydoc/375->
   <../../pages/mydoc/376->
