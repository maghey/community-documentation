Phishing and Suspicious Email Templates
=======================================

Here you'll find all of our email templates related to phishing and suspicious emails.
You can contribute to our documentation through our Gitlab repository.

.. toctree::
   :maxdepth: 1

   <../../pages/mydoc/209->
   <../../pages/mydoc/57->
